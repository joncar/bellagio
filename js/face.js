  window.fbAsyncInit = function() {
    FB.init({
      appId      : '334682533394587',
      xfbml      : true,
      version    : 'v2.2'
    });        
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_LA/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
   function share(url){             
       FB.login(function(response){
           FB.api(
            "me/feed",
            "POST",
            {              
              link:url,              
            },
            function (response) {
             console.log(response);   
            });     
       },{scope: 'read_stream,email'});                 
   }