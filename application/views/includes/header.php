<div id="firstbar"></div>
<header class="container-fluid">
    <!--- Contacto -->
    <div class="row" style='margin-bottom:30px;'>
        <div class="col-xs-6" style="padding-left:30px">
            <i class="fa fa-envelope"></i> <?= $this->db->get('ajustes')->row()->correo ?>
            <i class="fa fa-phone"></i> <?= $this->db->get('ajustes')->row()->telefono ?>
        </div>
        <div class="col-xs-6" align="right" style="font-size:22px;">
            <a href="<?= $this->db->get('ajustes')->row()->facebook ?>"><i class="fa fa-facebook-square"></i></a>
            <a href="<?= $this->db->get('ajustes')->row()->twitter ?>"><i class="fa fa-twitter-square"></i></a>
            <a href="<?= $this->db->get('ajustes')->row()->instagram ?>"><i class="fa fa-instagram"></i></a>
            <a href="<?= $this->db->get('ajustes')->row()->linkedin ?>"><i class="fa fa-linkedin"></i></a>
            <div class="dropdown" style="width: 160px;">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-shopping-cart"></i> Carrito
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <?php if(!empty($_SESSION['cart'])): ?>
                  <?php foreach($_SESSION['cart'] as $c): ?>
                    <li><a href="<?= base_url('main/cart') ?>"><?= img('files/'.$c->miniatura,'width:25%') ?> <?= substr($c->nombre,0,10) ?> <span class="badge"><?= $c->cantidad ?></span></a></li>        
                  <?php endforeach ?>
                  <?php else: ?>
                    <li><a href="#">Vacio</a></li>        
                  <?php endif ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <?php $this->load->view('includes/nav'); ?>
    </div>
</header>
