<?= $this->load->view('includes/nav'); ?>
<section class="container">
    <div class="row">
        <div class="col-xs-4 col-sm-1" style="padding:15px 0;"><?= img('img/'.$conf->foto,'width:100%; border-radius:100%') ?></div>
        <div class="col-xs-8 col-sm-10" style="text-align:right; padding:30px;">
            <? foreach($this->db->get('categorias')->result() as $c): ?>
                <a href="<?= site_url('blog/cat/'.$c->id) ?>"><?= $c->nombre ?></a> |            
            <? endforeach ?>
        </div>
    </div>
</section>
<section class="title"><h1><?= !empty($title)?$title:'Blog' ?></h1></section>
<ol class="breadcrumb">
  <li><a href="<?= site_url('blog') ?>">Blog</a></li>
  <?php if(!empty($bread)): ?>
  <? foreach($bread as $b): ?>
  <li><a href="<?= site_url('blog/'.$b[0]) ?>"><?= $b[1] ?></a></li>
  <? endforeach ?>
  <?php endif ?>
</ol>