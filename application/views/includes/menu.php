<nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">                  
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>                    
          </div>                  
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="visible-lg nav navbar-nav" align="center" style="width:100%;">
              <li><a href="<?= site_url('nosotros') ?>">NOSOTROS</a></li>
              <li><a href="<?= site_url('resena') ?>">RESEÑA</a></li>
              <li><a href="<?= site_url('productos') ?>">PRODUCTOS</a></li>
              <li><a href="<?= site_url('clientes') ?>">CLIENTES</a></li>
              <li><a href="<?= site_url('coleccion') ?>">COLECCION</a></li>
              <li><a href="<?= site_url('novedades') ?>">NOVEDADES</a></li>
              <li><a href="<?= site_url('contactenos') ?>">CONTACTENOS</a></li>
            </ul>
            <ul class="hidden-lg nav navbar-nav" align="center" style="width:100%;">              
              <li><a href="<?= site_url('nosotros') ?>">NOSOTROS</a></li>
              <li><a href="<?= site_url('resena') ?>">RESEÑA</a></li>
              <li><a href="<?= site_url('productos') ?>">PRODUCTOS</a></li>
              <li><a href="<?= site_url('clientes') ?>">CLIENTES</a></li>
              <li><a href="<?= site_url('coleccion') ?>">COLECCION</a></li>
              <li><a href="<?= site_url('novedades') ?>">NOVEDADES</a></li>                      
              <li><a href="<?= site_url('contactenos') ?>">CONTACTENOS</a></li>
            </ul> 
          </div>
        </div>
    </nav>
