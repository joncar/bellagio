<?php $this->load->view('includes/header') ?>
<?php if($this->router->fetch_class()=='main' || $this->router->fetch_class()=='blog'): ?>
<div class="row">
    <?php $this->load->view('predesign/owlcarousel') ?>
    <div class="owlcarousel owl-carousel">
        <?php foreach($this->db->get('banner')->result() as $i){
            if(empty($i->url))
            echo '<div class="item">'.img('img/'.$i->foto).'</div>';
            else
            echo '<a href="'.$i->url.'"><div class="item">'.img('img/'.$i->foto).'</div></a>';
        }
        ?>
    </div>
</div>
<?php endif ?>



<div class='row' style='padding:0px 30px; margin-top:30px; border-top:1px solid #999999; border-bottom: 1px solid #999999;'>
    <?= $entry->texto ?>
    <?php if($entry->titulo=='clientes'): ?>
    <?php $this->load->view('clientes'); ?>
    <?php endif ?>
</div>





<?php if($this->router->fetch_class()=='main'): ?>
<div class='row' style='padding:0 30px; margin-top:30px'>
    <div class='col-xs-12 col-sm-6'>
        <a href='<?= site_url('novedades') ?>'>
            <div class='row' style='border:1px solid #999999'>
                <div class='col-xs-6'>
                    <?= img('img/'.$this->db->get_where('blog',array('titulo'=>'novedades'))->row()->imagen,'width:100%;') ?>
                </div>
                <div class='col-xs-6'>
                    <h3 style='background: #f1c967; padding:5px;'><span style="color:white">Cartera Bellagio</span><div align="right" style="color:#004021">Novedades</div></h2>
                    <h3 align="right" style="color:#808080">
                        Nuestros últimos artículos
                    </h3>
                </div>
            </div>
        </a>
    </div>
    <div class='col-xs-12 col-sm-6'>
        <a href='<?= site_url('coleccion') ?>'>
        <div class='row' style='border:1px solid #999999'>
            <div class='col-xs-6'>
                <?= img('img/'.$this->db->get_where('blog',array('titulo'=>'coleccion'))->row()->imagen,'width:100%;') ?>
            </div>
            <div class='col-xs-6'>
                <h3 style='background: #f1c967; padding:5px;'><span style="color:white">Cartera Bellagio</span><div align="right" style="color:#004021">Colección</div></h2>
                <h3 align="right" style="color:#808080">
                    Nuestra última colección
                </h3>
            </div>
        </div>
        </a>
    </div>
</div>
<?php endif ?>
<?php $this->load->view('includes/footer'); ?>
