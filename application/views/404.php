<?php $this->load->view('includes/header') ?>
<section class="container" style="padding:40px 0;">
    <div class="row">
        <div class="col-xs-12">
            <h1>URL no encontrada</h1>
            <p>Parece que la web que intentas acceder no esta disponible, o no se encuentra en nuestro servidor. </p>
        </div>
    </div>
</section>
