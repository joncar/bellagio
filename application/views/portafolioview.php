<div class="blog">
<?= $this->load->view('includes/nav'); ?>    
    <div class="row">
        <section class="title"><h1><?= $entry->nombre ?></h1></section>
        <section class="container" style="padding:40px 0;">
            <div class="row">
                <div class="col-xs-4">
                    <div id="foto"><?= img('img/'.$entry->foto,'width:100%; height:500px;') ?></div>
                    <div id="fotoslist" style="margin-top:20px">
                        <div style="padding:0px; cursor: pointer;" class="col-xs-2 col-sm-1 active"><?= img('img/'.$entry->foto,'width:100%; height:30px;') ?></div>
                        <?php foreach($fotos->result() as $f): ?>
                        <div style="padding:0px; cursor: pointer;" class="col-xs-2 col-sm-1"><?= img('img/'.$f->foto,'width:100%;  height:30px;') ?></div>
                        <?php endforeach ?>
                    </div>
                </div>
                <div class="col-xs-8 well">
                    <b>Dispositivo: </b><?= $entry->plataforma ?> | <b>Plataforma: </b><?= $entry->tipo ?>
                    <div style="margin-top:20px">
                        <p><b>Descripción</b></p>
                        <?= $entry->descripcion ?>
                        <p>
                            <a href="<?= site_url($entry->demo) ?>">Demo</a> | 
                        </p>
                    </div>
                </div>
            </div>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- banner superior -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px"
                 data-ad-client="ca-pub-9044371564984712"
                 data-ad-slot="1748039809"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </section>
        <script>
            $(document).ready(function(){        
               $("header nav").css('background','#333');
               
               var active = '';          
                active = $("#foto img").attr('src');
                $("#fotoslist div").mouseover(function(){
                    $(this).addClass('active');     
                    $("#foto img").attr('src',$(this).find('img').attr('src'))
                });

                $("#fotoslist div").mouseout(function(){
                    if(active!=$(this).find('img').attr('src'))
                        $(this).removeClass('active');
                    $("#foto img").attr('src',active)
                });

                $("#fotoslist div").click(function(){
                    $("#fotoslist div").removeClass('active');
                    $(this).addClass('active');
                    $("#foto img").attr('src',$(this).find('img').attr('src'));
                    active = $(this).find('img').attr('src');
                });  
            })
        </script>
    </div>
</div>