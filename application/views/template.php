<!DOCTYPE html>
<html>
    <head>
        <title><?= empty($title)?$conf->title:$title ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta property="og:image" content="<?= empty($thumbnail)?'':$thumbnail ?>" />
        <meta property="og:site_name" content="<?= empty($title)?$conf->title:$title ?>" />
        <?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; ?>                
                <?php endif; ?>                                
        <? if(empty($crud)): ?><script src="http://code.jquery.com/jquery-1.9.0.js"></script><? endif ?>        
        <script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/frame.js') ?>"></script>
        <script src="<?= base_url('js/face.js') ?>"></script>
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/font-awesome/css/font-awesome.css') ?>">                
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/bootstrap.min.css') ?>">                
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">                        
    </head>
    <body>        
        <?= $this->load->view($view) ?>
    </body>
</html>
