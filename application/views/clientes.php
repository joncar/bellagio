<div class='col-xs-12 col-sm-4'>
    <?= $this->load->view('predesign/login'); ?>
</div>
<div class='col-xs-12 col-sm-4 col-sm-offset-3'>
    <div class='well'>
        <h1>Registrate</h1>
        <form id="myform" onsubmit="return sendReg();">
                <div class="form-group">  
                  <div class="input-group">
                    <input name="nombre" type="text" class="form-control" id="field-nombre" placeholder="Nombre" aria-describedby="field-nombre">                        
                    <span class="input-group-addon"><i class="glyphicon"></i></span>
                  </div>
                </div>
                <div class="form-group">  
                  <div class="input-group">
                    <input name="email" type="email" class="form-control" id="field-email" placeholder="Correo Electrónico">
                    <span class="input-group-addon"><i class="glyphicon"></i></span>
                  </div>                      
                </div>
                <div class="form-group">   
                  <div class="input-group">
                    <input name="password" type="password" class="form-control" id="field-password" placeholder="Contraseña que usarás en el sistema">
                    <span class="input-group-addon"><i class="glyphicon"></i></span>
                  </div>                      
                </div>                                
                <div align="center"><button type="submit" class="btn btn-success">Registrarme</button></div>
          </form>
    </div>
</div>
<script>
    function sendReg(){
        form = new FormData(document.getElementById('myform'));
        $.ajax({
            url:'<?= base_url('registro/index/insert_validation') ?>',
            data: form,
            type:'post',
            contentType:false,
            processData:false,
            success:function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    $.ajax({
                        url:'<?= base_url('registro/index/insert') ?>',
                        data:form,
                        type:'post',
                        contentType:false,
                        processData:false,
                        success:function(data){
                            data = data.replace('<textarea>','');
                            data = data.replace('</textarea>','');
                            data = JSON.parse(data);
                            if(data.success){
                                document.location.href="<?= base_url('panel') ?>";
                            }
                            else {
                                alert("Ocurrio un error");
                            }
                        }
                    });
                }
                else{
                    emergente(data.error_message);
                    for(i in data.error_fields){
                        $("#field-"+i).parent().addClass('has-error');
                        $("#field-"+i).parents('.input-group').find('.glyphicon').addClass('glyphicon-remove');
                    }
                }
            }
        });
        return false;
    }
</script>
