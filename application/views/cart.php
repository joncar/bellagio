<?php $this->load->view('includes/header') ?>
<div class='row'>    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">Información de tu pedido</h1>
        </div>
        <div class="panel-body">            
            <div class="col-xs-9">
            <?php if(!empty($_SESSION['cart'])): ?>            
              <div class="row">
                    <?php foreach($_SESSION['cart'] as $e): ?>
                        <div class="col-xs-6 col-sm-3 productos">
                            <?= img('files/'.$e->miniatura,'width:100%'); ?>
                            <div class="well">
                                <div style="height:40px;"><b><?= substr($e->nombre,0,40).'...' ?></b></div>
                                <!--<div align='right'>Precio: <span style='color:red'></span></div>-->
                                <div align="right"><span class="badge badge-default"><?= $e->cantidad ?></span> <a href="<?= site_url('productos/entry/'.$e->id.'-'.str_replace("+","-",urlencode($e->nombre))) ?>">Ver más detalles </a> <a class="btn btn-default fa fa-eraser" href="<?= base_url('main/removecart/'.$e->id) ?>">Quitar</a></div>
                            </div>
                        </div>
                    <?php endforeach ?>
              </div>            
            <?php else: ?>
              <div>Vacio</div>
            <?php endif ?>
           </div>
            <div class="col-xs-3">
                <a href="<?= base_url('panel/procesar_pedido') ?>" class="btn btn-success btn-lg"><i class="fa fa-check-circle"></i> Procesar Pedido</a>
            </div>
        </div>
    </div>
</div>