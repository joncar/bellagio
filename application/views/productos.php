<?= $this->load->view('includes/header') ?><div class='container' style='margin-top:20px'>
        <div class="row">
            <div class="col-xs-12 col-sm-2 sidebar">
                <h2>Categorias</h2>
                <form action="<?= site_url('productos') ?>" method="get">
                    <ul class="categorias">
                        <? foreach($categorias->result() as $c): ?>
                        <li><a href="<?= site_url('productos/cat/'.$c->id.'-'.str_replace('+','-',urlencode($c->nombre))) ?>"><?= $c->nombre ?> (<?= $c->entry ?>)</a></li>
                        <? endforeach ?>                                            
                    </ul>                
                    <label for="filter">Buscar: </label>
                 <input type="search" name="filter" id="filter" class="form-control" placeholder="Nombre o Referencia" value="<?= !empty($_GET['filter'])?$_GET['filter']:'' ?>">
                </form>
            </div>
            <div class="col-xs-12 col-sm-10 contenido">
                <?php if(!empty($entrys)): ?>
                <?= $entrys->num_rows==0?'No existen productos para esta categoria':'' ?>
                <div class="row">
                    <?php foreach($entrys->result() as $e): ?>
                        <a href="<?= site_url('productos/entry/'.$e->id.'-'.str_replace("+","-",urlencode($e->nombre))) ?>" class="col-xs-6 col-sm-3 productos">
                            <?= img('files/'.$e->miniatura,'width:100%'); ?>
                            <div class="well">
                                <div style="height:40px;"><b><?= substr($e->nombre,0,40).'...' ?></b></div>
                                <!--<div align='right'>Precio: <span style='color:red'></span></div>-->
                                <div align="right">Ver más</div>
                            </div>
                        </a>
                    <?php endforeach ?>
                </div>
                <?php endif ?>
                <?php if(!empty($entry)): ?>
                <?php $fotos = $this->prodmodel->getFotos($entry->id) ?>
                <div class="row prodentry">
                    <div class='col-xs-12 col-sm-7'>
                        
                        <div class='row' id='foto'>
                            <?= img('files/'.$entry->miniatura,'width:100%') ?>
                        </div>
                        <div id='row'>
                            <div class='col-xs-1 miniatura active'><?= img('files/'.$entry->miniatura,'width:100%') ?></div>
                            <?php foreach($fotos->result() as $f): ?>
                            <div class='col-xs-1 miniatura'><?= img('files/'.$f->foto,'width:100%') ?></div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class='col-xs-12 col-sm-5 well'>
                        <h1 class='row'>
                            <div class='col-xs-12 col-sm-12'><?= $entry->nombre ?></div>
                        </h1>
                        <p>
                            <b>Medidas: </b> <?= $entry->medidas ?>
                        </p>
                        <p>
                            <b>Referencia: </b> <?= $entry->descripcion ?>
                        </p>
                        <p>
                            <b>Colores: </b> <?= $entry->colores ?>
                        </p>
                        <p align="right">
                            <a href="<?= base_url('main/addcart/'.$entry->id) ?>" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Añadir al carrito</a>
                        </p>
                    </div>
                </div>
                <?php $this->db->order_by('id','DESC') ?>
                <?php $this->db->limit('4') ?>
                <?php $this->db->where('categoria',$entry->categoria) ?>
                <?php $this->db->where('productos.id !=',$entry->id) ?>
                <?php $related = $this->prodmodel->getEntrys() ?>
                <?php if($related->num_rows>0): ?>
                <div class='row related'>
                    <h1>Productos relacionados</h1>
                    <div class='row'>
                    <?php foreach($related->result() as $r): ?>
                        <a href="<?= site_url('productos/entry/'.$r->id.'-'.str_replace("+","-",urlencode($r->nombre))) ?>" class="col-xs-6 col-sm-2 productos">
                            <?= img('files/'.$r->miniatura,'width:100%'); ?>
                            <div class="well">
                                <div><b><?= $r->nombre ?></b></div>
                                <div align='right'>Ver más</div>
                            </div>
                        </a>
                    <?php endforeach ?>
                    </div>
                </div>
                <?php endif ?>
                <?php endif ?>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        $(".miniatura").hover(function(){
            $("#foto img").attr('src',$(this).find('img').attr('src'));
            $(".miniatura").removeClass('active');
            $(this).addClass('active');
        })
    });
</script>
