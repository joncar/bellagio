<link href="<?= base_url('assets/owl/owl.carousel.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/owl/owl.carousel.js') ?>"></script>
<style>
.owlcarousel .item{
    margin: 3px;
}
.owlcarousel .item img{
    display: block;
    width: 100%;
    height: auto;
}
</style>
<script>
$(document).ready(function() {
  $(".owlcarousel").owlCarousel({
    autoPlay: 3000,
    items : 4,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3]
  });

});
</script>
<!--- Template 
    <div class="owlcarousel owl-carousel">
        <div class="item"><img src="assets/owl1.jpg" alt="Owl Image"></div>
        <div class="item"><img src="assets/owl2.jpg" alt="Owl Image"></div>
        <div class="item"><img src="assets/owl3.jpg" alt="Owl Image"></div>
        <div class="item"><img src="assets/owl4.jpg" alt="Owl Image"></div>
        <div class="item"><img src="assets/owl5.jpg" alt="Owl Image"></div>
        <div class="item"><img src="assets/owl6.jpg" alt="Owl Image"></div>
        <div class="item"><img src="assets/owl7.jpg" alt="Owl Image"></div>
        <div class="item"><img src="assets/owl8.jpg" alt="Owl Image"></div>
    </div>     
-->
