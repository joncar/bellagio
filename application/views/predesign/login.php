<? if(empty($_SESSION['user'])): ?>
<? if(!empty($msj))echo $msj ?>
<? if(!empty($_SESSION['msj']))echo $_SESSION['msj'] ?>
<form role="form" class="form-horizontal well" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">
   <h1 align='center'>Iniciar sessión</h1>
   <?= input('usuario','Email','text') ?>
   <?= input('pass','Contraseña','password') ?>
   <input type="hidden" name="redirect" value="<?= empty($_GET['redirect'])?base_url('panel'):base_url($_GET['redirect']) ?>">
   <div><input type="checkbox" name="remember" value="1" checked> Recordar contraseña</div><br/>
   <div align="center"><button type="submit" class="btn btn-success">Ingresar</button>  <br/>
   <a class="btn btn-link" href="<?= base_url('main/forget') ?>">¿Olvidó su contraseña?</a>   
   </div>
</form>
<? else: ?>
<div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large">Entrar al sistema</a></div>
<? endif; ?>
<?php $_SESSION['msj'] = null ?>