<!-- Caja de comentarios de facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&appId=334682533394587&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Boton de shared -->
<div class="blog">
<? $this->load->view('includes/blog_header'); ?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- banner superior -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9044371564984712"
     data-ad-slot="1748039809"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<section class="container" style="padding:40px 0;">
    <div class="row">
        <div class="col-xs-8">
            <div align="right">
                <div class="fb-share-button" data-href="<?= site_url('blog/'.str_replace("+","-",urlencode($entry->titulo))); ?>" data-layout="button_count"></div>
                <a class="twitter-share-button"
                    href="https://twitter.com/share">
                  Tweet
                  </a>
                  <script>
                  window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
                  </script>
                <!-- Redes sociales -->
                <a class="twitter-follow-button"
                href="https://twitter.com/joncar1589"
                data-show-count="false"
                data-lang="es">
                    Sigeme @joncar1589
                </a>                
              <script>window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));</script>
            </div>
            <?= $entry->texto ?>
            <div align="right"><?= date("d/m/Y",strtotime($entry->fecha)) ?></div>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- banner superior -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:728px;height:90px"
                 data-ad-client="ca-pub-9044371564984712"
                 data-ad-slot="1748039809"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            <!-- Otros posts -->
            <?php $this->db->order_by('id','DESC'); $otherentrys = $this->db->get_where('blog',array('id !='=>$entry->id)) ?>
            <?php if($otherentrys->num_rows>0): ?>
            <div class="row">
                <div class="col-xs-12"><h2>Otros Posts</h2></div>                
            </div>
            <div class="row">
                <?php for($i=0;$i<4;$i++): ?>
                    <div class="col-xs-3"><a href="<?= site_url('blog/'.str_replace("+","-",urlencode($otherentrys->row($i)->titulo))) ?>"><div class="blog_miniatura" style="background:url(<?= base_url('img/'.$otherentrys->row($i)->imagen) ?>) no-repeat; background-size:100%;"></div><b><?= $otherentrys->row($i)->titulo ?></b></div></a>
                <?php endfor ?>                                
            </div>
            <?php endif ?>
            <!-- Comenarios de facebook -->
            <div class="fb-comments" data-href="<?= site_url('blog/'.str_replace("+","-",urlencode($entry->titulo))) ?>" data-width="100%" data-numposts="20" data-colorscheme="light"></div>            <
        </div>
        <div class="col-xs-4">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- aside -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:300px;height:600px"
                 data-ad-client="ca-pub-9044371564984712"
                 data-ad-slot="1608439005"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){        
       $("header nav").css('background','#333');
    })
</script>
</div>