<?php $this->load->view('includes/header') ?>
<h2>Contactenos</h2>
<div class="row">
    <div class="col-xs-6">
        <form onsubmit="return send(this,'<?= base_url('main/contactenos') ?>')">
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre">
          </div>
          <div class="form-group">
            <label for="telefono">Telefono</label>
            <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Correo Electrónico">
          </div>
           <div class="form-group">
            <label for="nombre">Mensaje</label>
            <textarea class="form-control" name="mensaje" id="mensaje" placeholder="Mensaje"></textarea>
          </div>
            <div align="center"><button type="submit" class="btn btn-success">Enviar solicitud de contacto</button></div>
        </form>
     </div>
    <div class="col-xs-6">
        <p><b>Dirección</b></p>
        Calle Principal El Marquez Local Nro. S/N - 3 Urb. El Marquez Guatire Miranda Zona Postal 1221.
        <p><b>Teléfonos</b></p>
         +58 212 3448411 | 3448011 | 4179055.
         <p><b>Correo electrónico</b></p> ventas@bellagiobags.com
    </div>
</div>
