<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Clientes extends Panel {
    public function __construct()
    {
            parent::__construct();                                  
    }
    
    public function presupuestos($x = '',$y = '')
    {
        $crud = parent::presupuestos();
        
        
        if($x=='add'){
            $crud->callback_field('detalles',function($val){
                $str = '';
                
                foreach($this->db->get('categorias_productos')->result() as $c){
                    $str.= '<div><b>'.$c->nombre.'</b></div>';
                    foreach($this->db->get_where('productos',array('categoria'=>$c->id))->result() as $p){
                        $str.= '<div class="productlistitem"><input type="checkbox" class="productlist" value="'.$p->nombre.'"> '.$p->nombre.' | Cantidad: <input type="number" value="1" disabled></div>';
                    }
                    return $str.'<input id="field-detalles" type="hidden" name="detalles" value="">';
                }
            });
        }
        $crud->unset_fields('status');
        $crud->unset_columns('cliente')
             ->unset_edit()
             ->unset_delete();
        $output = $crud->render();
        $output->view = 'panel';
        $output->crud = 'presupuesto';
        $output->menu = 'presupuesto';
        $output->title = 'Presupuesto';
        $this->loadView($output);        
    }
}