<?php
require_once('main.php');
class Productos extends Main{
    function __construct()
    {        
        parent::__construct();        
    }
    
    function index()
    {
        $this->loadView(array('view'=>'productos','title'=>'Productos','entrys'=>$this->prodmodel->getEntrys(),'categorias'=>$this->prodmodel->getCats()));
    }
    
    function cat($x)
    {
        $x = explode("-",$x);
        if(is_numeric($x[0]) && $this->db->get_where('categorias_productos',array('id'=>$x[0]))->num_rows>0){
            $this->db->where('categoria',$x[0]);
            $this->index();
        }
        else
        $this->loadView('404');
    }
    
    function entry($x)
    {
        $x = explode("-",$x);
        if(is_numeric($x[0])){
            $this->db->where('productos.id',$x[0]);
            $entry = $this->prodmodel->getEntrys();
            if($entry->num_rows>0){
                $this->loadView(array('view'=>'productos','title'=>$entry->row()->nombre,'categorias'=>$this->prodmodel->getCats(),'entry'=>$entry->row()));
            }
            else
                $this->loadView('404');
        }
    }
    
    function is_entry($id)
    {
        return $this->db->get_where('productos',array('id'=>$id))->num_rows>0?TRUE:FALSE;
    }
    
    /*************************** CRUDS ************************************/
    /****************************** CALLBACKS **************************/
    /******************************* Ajax Request ***************************************/
}
?>