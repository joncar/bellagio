<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {

        public function __construct()
        {
                parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url());
                $this->load->library('image_crud');

                if($_SESSION['cuenta']!=1 && ($this->router->fetch_class()!='clientes' && $this->router->fetch_method()!='index'))
                    header("Location:".base_url('panel'));
        }

        public function index($url = 'main',$page = 0)
        {
                $this->loadView('panel');
        }

        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('blog/index/clientes/?redirect='.$_SERVER['REQUEST_URI']));
            else
            parent::loadView($crud);
        }                
        /*Cruds*/       
        public function user()
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('user');               
                $crud->set_subject('Usuario');                                                
                $crud->required_fields('email','nombre','password');
                $crud->set_rules('email','Email','required|valid_email');
                $crud->field_type('status','true_false',array('0'=>'Inactivo','1'=>'Activo'));
                $crud->callback_before_update(function($row,$id){
                    $user = get_instance()->db->get_where('user',array('id'=>$id))->row()->password;
                    if($user!=md5($post['password']))
                        $post['password'] = md5($post['password']);
                    return $post;
                });
                $crud->field_type('cuenta','true_false',array(0=>'Cliente',1=>'Admin'));
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'parciales';
                $output->title = 'Puntajes de parciales';
                $this->loadView($output);
                return $crud;
        }

        public function paginas()
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('paginas');               
                $crud->set_subject('Paginas');  
                $crud->required_fields('url');
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'parciales';
                $output->title = 'Registro de paginas';
                $this->loadView($output);
                return $crud;
        }

        public function habilidades()
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('habilidades');               
                $crud->set_subject('Habilidades');  
                $crud->required_fields('titulo');
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'parciales';
                $output->title = 'Registro de habilidades';
                $this->loadView($output);
                return $crud;
        }

        public function portafolio()
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('portafolio');               
                $crud->set_subject('Portafolio');  
                $crud->required_fields('nombre','foto'); 
                $crud->field_type('plataforma','enum',array('Movil','WEB'));
                $crud->field_type('tipo','enum',array('Android','IOS','Desktop/Mobile','Android/IOS'));
                $crud->set_field_upload('foto','img');
                $crud->columns('nombre','fotos','redes');
                $crud->callback_column('fotos',function($val,$row){
                    return '<a href="'.base_url('panel/portafolio_fotos/'.$row->id).'" >Administrar</a>';
                });
                $crud->callback_column('redes',function($val,$row){
                    return '<a href="javascript:share(\''.site_url('portafolio/'.str_replace("+","-",urlencode($row->nombre))).'\')"><i class="fa fa-facebook"></i></a>';
                });                
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'parciales';
                $output->title = 'Registro de portafolio';
                $this->loadView($output);
                return $crud;
        }

        public function portafolio_fotos()
        {
            $image_crud = new image_CRUD();
            $image_crud->set_primary_key_field('id');
            $image_crud->set_url_field('foto');
            $image_crud->set_table('portafolio_fotos')
            ->set_relation_field('portafolio')
            ->set_ordering_field('priority')
            ->set_image_path('img');

            $output = $image_crud->render();
            $output->view = 'panel';
            $output->crud = 'user';            
            $this->loadView($output);
        }

        public function clientes()
        {
                        $crud = new ajax_grocery_CRUD();
                        $crud->set_theme('flexigrid');
                        $crud->set_table('clientes');               
                        $crud->set_subject('Clientes');  
                        $crud->required_fields('nombre','opinion'); 
                        $crud->set_field_upload('foto','img');
                        $output = $crud->render();
                        $output->view = 'panel';
                        $output->crud = 'user';
                        $output->menu = 'parciales';
                        $output->title = 'Registro de clientes';
                        $this->loadView($output);
                        return $crud;
        }

        public function ajustes()
        {
                        $crud = new ajax_grocery_CRUD();
                         $crud->set_theme('flexigrid');
                         $crud->set_table('ajustes');               
                         $crud->set_subject('Ajuste');                                                                
                         $crud->unset_delete()
                              ->unset_add()
                              ;  
                         $crud->set_field_upload('foto','img');
                         $output = $crud->render();
                         $output->view = 'panel';
                         $output->crud = 'user';
                         $output->menu = 'parciales';
                         $output->title = 'Ajustes generales';
                         $this->loadView($output);
                         return $crud;
        }

        public function categorias()
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('categorias');
                $crud->set_subject('Categorias');
                $crud->unset_add();
                $crud->required_fields('nombre');
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'parciales';
                $output->title = 'Categorias de Blog';
                $this->loadView($output);
                return $crud;
        }

        public function blog($x = '',$y = '')
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('blog');
                $crud->set_subject('Entrada');
                $crud->set_relation('categoria','categorias','nombre');
                $crud->required_fields('titulo','texto','categoria');
                $crud->set_field_upload('imagen','img');
                $crud->field_type('fecha','invisible');                
                $crud->callback_before_insert(function($post){$post['fecha'] = date("Y-m-d"); return $post;});
                if(empty($y) && !empty($_POST['titulo']) || !empty($y) && !empty($_POST['titulo']) && $_POST['titulo']!=$this->db->get_where('blog',array('id'=>$y))->row()->titulo){
                    $crud->set_rules('titulo','Titulo','required|is_unique[blog.titulo]');
                }                
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'parciales';
                $output->title = 'Blog';
                $this->loadView($output);
                return $crud;
        }

        public function images($x = '',$y = '')
        {
              $crud = new ajax_grocery_CRUD();
                        $crud->set_theme('flexigrid');
                        $crud->set_table('banner');
                        $crud->set_subject('Imagen');                
                        $crud->required_fields('foto','url');
                        $crud->set_field_upload('foto','img');                
                        $output = $crud->render();
                        $output->view = 'panel';
                        $output->crud = 'user';
                        $output->menu = 'banner';
                        $output->title = 'Banner';
                        $this->loadView($output);
                        return $crud;
        }

        public function categorias_productos($x = '',$y = '')
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('categorias_productos');
                $crud->set_subject('Categoria');                
                $crud->required_fields('nombre');                
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'banner';
                $output->title = 'Banner';
                $this->loadView($output);
                return $crud;
        }

        public function productos($x = '',$y = '')
        {
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('productos');
                $crud->set_subject('Producto'); 
                $crud->set_relation('categoria','categorias_productos','nombre');
                $crud->required_fields('nombre','descripcion','categoria');
                $crud->set_field_upload('miniatura','files');  
                $crud->display_as('descripcion','Referencia');
                $crud->columns('categoria','nombre','precio','descripcion','miniatura','fotos');
                $crud->callback_column('fotos',function($val,$row){
                    return '<a href="'.base_url('panel/fotos/'.$row->id).'">Administrar</a>';
                });
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'banner';
                $output->title = 'Banner';
                $this->loadView($output);
                return $crud;
        }

        public function fotos($x = '',$y = '')
        {
            $image_crud = new image_CRUD();
            $image_crud->set_primary_key_field('id');
            $image_crud->set_url_field('foto');
            $image_crud->set_table('fotos')
                ->set_relation_field('producto')
                ->set_ordering_field('priority')
                ->set_image_path('files');

            $output = $image_crud->render();
            $output->view = 'panel';
            $output->crud = 'user';      
            $output->output = '<a class="btn btn-default" href="'.site_url('panel/productos').'">Volver al area de productos</a>'.$output->output;
            $this->loadView($output);
        }

        public function presupuestos($x = '',$y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('presupuestos');
            $crud->set_subject('Presupuesto');    
            $crud->field_type('status','hidden',0);
            if($_SESSION['cuenta']==0)$crud->field_type ('cliente','hidden',$_SESSION['user']);
            else $crud->set_relation('cliente','user','nombre');
            $crud->field_type('status','true_false',array('0'=>'Pendiente','1'=>'Respondido'));
            $crud->required_fields('cliente','detalles');
            if($this->router->fetch_class()=='panel'){
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'presupuesto';
                $output->title = 'Presupuesto';
                $this->loadView($output);
            }
            else
            return $crud;
        }
        
        public function procesar_pedido(){
            echo 'asd';
        }
}   


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
