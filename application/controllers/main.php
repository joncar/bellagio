<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_start();
class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */        
	public function __construct()
	{
		parent::__construct();                
		$this->load->helper('url');
                $this->load->helper('html');
                $this->load->helper('h');                
                $this->load->database();
                $this->load->model('user');                        
                $this->load->model('querys');  
                $this->load->model('prodmodel');
                $this->load->library('grocery_crud');                
                $this->load->library('ajax_grocery_crud');
                ini_set('display_errors',true);
                
                if($this->db->get_where('visitas',array('ip'=>$_SERVER['REMOTE_ADDR']))->num_rows==0)
                    $this->db->insert('visitas',array('ip'=>$_SERVER['REMOTE_ADDR']));
	}
        
        public function index($id = '')
	{                        
            $this->loadView(array('view'=>'main','entry'=>$this->db->get_where('blog',array('titulo'=>'Nosotros'))->row()));         
	}
        
        public function addcart($id){
            if(!empty($_SESSION['cart'])){
                $enc = false;
                foreach($_SESSION['cart'] as $n=>$c){
                    if($c->id==$id){
                        $enc = true;
                        $_SESSION['cart'][$n]->cantidad+=1;
                        header("Location:".base_url('productos/entry/'.$id.'/'.str_replace("+","-",urlencode($entry->nombre))));
                    }
                }
                if(!$enc){
                    $this->db->where('productos.id',$id);
                    $entry = $this->prodmodel->getEntrys();
                    if($entry->num_rows>0){
                        $entry = $entry->row();
                        $entry->cantidad = 1;
                        $_SESSION['cart'][] = $entry;
                        header("Location:".base_url('productos/entry/'.$id.'/'.str_replace("+","-",urlencode($entry->nombre))));
                    }                        
                    else header("Location:".base_url());
                }
            }
            else{
                $this->db->where('productos.id',$id);
                $entry = $this->prodmodel->getEntrys();
                if($entry->num_rows>0){
                    $entry = $entry->row();
                    $entry->cantidad = 1;
                    $_SESSION['cart'][] = $entry;
                    header("Location:".base_url('productos/entry/'.$id.'/'.str_replace("+","-",urlencode($entry->nombre))));
                }                        
                else header("Location:".base_url());
            }
        }
        
        function removecart($id){
            if(!empty($_SESSION['cart'])){                
                foreach($_SESSION['cart'] as $n=>$c){
                    if($c->id==$id){                        
                        unset($_SESSION['cart'][$n]);
                        header("Location:".base_url('main/cart/'));
                    }
                }                
            }
            else header("Location:".base_url());
        }
        
        public function cart(){
            $this->loadView('cart');
        }
        
        public function admin()
        {
            $this->loadView('panel');
        }
        
        public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}
        
        public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['usuario']) && !empty($_POST['pass']))
			{
				$this->db->where('email',$this->input->post('usuario'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('usuario',TRUE),$this->input->post('pass',TRUE)))
				{
					if($r->num_rows>0 && $r->row()->status==1)
					{
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('panel').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
					}
					else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
				}
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
			}
			else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');
                        
                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url('admin'));
		}
	}

	public function unlog()
	{
		$this->user->unlog();                
                header("Location:".site_url());
	}
        
        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param))
            $param = array('view'=>$param,'conf'=>$this->querys->get_conf());
            elseif(is_array($param))
            $param['conf'] = $this->querys->get_conf();
            else
            $param->conf = $this->querys->get_conf();
            $this->load->view('template',$param);
        }
		
	public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
        
        function sendmsj()
        {
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            if($this->form_validation->run())                
            {
                $email = $this->querys->get_conf()->email_contacto;
                $msj = '';
                foreach($_POST as $i=>$p){
                    $msj.= '<p><b>'.$i.'</b>: '.$p.'</p>';
                }
                correo($email,$_POST['motivo'],$msj);
                echo $this->success('Su correo ha sido enviado, en breve le contactare');
            }
            else echo $this->error($this->form_validation->error_string());
        }
        
        function contactenos(){
            if(empty($_POST)){
                $this->loadView(array('view'=>'contactenos','title'=>'Contactenos'));
            }
            else{
                foreach($_POST as $n=>$p)
                    $this->form_validation->set_rules($n,ucfirst($n),'required');
                if($this->form_validation->run()){
                    $p = '';
                    foreach($_POST as $n=>$f){
                        $p.= '<p><b>'.$n.'</b>: '.$f.'</p>';
                    }
                    
                      correo('ventas@bellagiobags.com','Solicitud de contacto',$p);
                      correo('administracion@bellagiobags.com','Solicitud de contacto',$p);
                      correo('facturacion.bellagio@yahoo.com','Solicitud de contacto',$p);
                      echo $this->success('Solicitud enviada');
                }
                else echo $this->error($this->form_validation->error_string());
            }
        }
        
        function sendMailer(){
            correo('joncar.c@gmail.com','test','test');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
