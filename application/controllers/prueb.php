<?php 
class Prueb extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->database(); //Aqui se carga el driver de la bd.
        $this->load->library('grocery_crud'); //Aqui se hace la carga de la libreria
    }

    function index($param1 = '',$param2 = ''){
        $this->load->view('main',array('title'=>'Hola Mundo'));
    }
    
    function prueba(){
        $crud = new grocery_crud();
        $crud->set_theme('flexigrid'); //Aqui se selecciona la vista del crud. 
        $crud->set_table('prueba'); //Se hace la seleccion de la tabla
        $crud->set_subject('Prueba'); //Se le asigna un alias al crud
        $crud->required_fields('nombre','apellido','email');
        $crud->set_rules('email','Email','required|valid_email');
        $crud->callback_before_insert(function($post){
            $post['password'] = md5($post['password']);
            return $post;
        });
        
        $crud->set_field_upload('nombre','img');
        $output = $crud->render();
        $this->load->view('crud',$output); 
    }
}
?>