<?php
class Querys extends CI_Model{
    function __construct(){
        parent::__construct();        
    }
    
    function get_conf()
    {
        $conf = $this->db->get_where('ajustes')->row();
        $conf->title = $conf->titulo_pagina;
        return $conf;
    }
    
    function get_page($url)
    {
        return $this->db->get_where('paginas',array('url'=>$url))->row();
    }
    
    function get_table($table){
        return $this->db->get_where($table)->result();
    }
}