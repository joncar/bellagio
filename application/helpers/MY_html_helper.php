<?php
function input($name = '',$label = '',$type='text')
{
    return '<div class="form-group">
              <label for="email" class="control-label">'.$label.'</label>
               
                <input type="'.$type.'" class="form-control" name="'.$name.'" id="field-'.$name.'" data-val="required" placeholder="'.$label.'">
                
             </div>';
}

function form_dropdown_from_query($name,$query,$value,$label,$first = 0,$extra = '',$chosen = TRUE,$class = '',$placeholder = 'Todos')
{
    if(is_string($query))$query = get_instance()->db->get($query);
    $data = array(''=>$placeholder);          
    foreach($query->result() as $q){
        if(count(explode(' ',$label))>0)
        {
            $data[$q->{$value}] = '';
            foreach(explode(' ',$label) as $l)
              $data[$q->{$value}].= $q->{$l}.' ';
        }
        else
        $data[$q->{$value}] = $q->{$label};
    }
    if($chosen)
    return form_dropdown($name,$data,$first,'class="form-control chosen-select '.$class.'" data-placeholder="'.$name.'" '.$extra);
    else
    return form_dropdown($name,$data,$first,'class="form-control '.$class.'" data-placeholder="'.$name.'" '.$extra);
}

function dropdown_query($nombre,$query,$first='0',$style='')
{
    
    $data = array();
    foreach($query->result() as $q)
    $data[$q->id] = $q->nombre;
    
    return '<div class="form-group">
              <label for="email" class="col-sm-2 control-label">'.$nombre.'</label>
               <div class="col-sm-10">
                    '.form_dropdown($nombre,$data,$first,$style).'
                </div>
             </div>';
}

function img($src = '',$style = '',$url = TRUE)
{
    $path = $url?base_url():'';
    $src = empty($src)?'img/vacio.png':$src;
    return '<img src="'.$path.$src.'" style="'.$style.'">';
}
?>
