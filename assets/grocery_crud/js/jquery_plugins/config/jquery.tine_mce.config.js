	$(function() {
		var tinymce_path = default_texteditor_path+'/tiny_mce/';
	
		var tinymce_options = {

				// Location of TinyMCE script
				script_url : tinymce_path +"tiny_mce.js",
				
				// General options
				theme : "advanced",
				plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,jbimages",
                                                                        relative_urls:false,
				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,jbimages,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,ltr,rtl,|,fullscreen",
				theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				entity_encoding : "raw",
				template_external_list_url : tinymce_path +"lists/template_list.js",
				external_link_list_url : tinymce_path +"lists/link_list.js",
				external_image_list_url : tinymce_path +"lists/image_list.js",
				media_external_list_url : tinymce_path +"lists/media_list.js",
                                                                        content_css : ["http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"],
				// Replace values for the template plugin
				template_replace_values : {
					username : "Some User",
					staffid : "991234"
				},
                                                                        extended_valid_elements: 'span[!class]',
                                                                        cleanup_on_startup: false,
                                                                        trim_span_elements: false,
                                                                        verify_html: false,
                                                                        cleanup: false,
                                                                        convert_urls: false
			};
		
		$('textarea.texteditor').tinymce(tinymce_options);
		
		var minimal_tinymce_options = $.extend({}, tinymce_options);
		minimal_tinymce_options.theme = "simple";
		
		$('textarea.mini-texteditor').tinymce(minimal_tinymce_options);
		
	});
